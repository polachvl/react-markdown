import React from 'react';
import Heading from './components/Heading';
import "./css/style.css";
let marked = require("marked");

export default class App extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			markdown: "# Zkus si mě \n" +
				"## Há dvojka \n" +
				"[Syntaxe](http://www.edgering.org/markdown/) \n \n" +
				"Tohle bude **tučně** *(ne to mládě od tučnáka)*. \n" +
				"* tu \n" +
				"* čně \n \n" +
				"![Tučně](https://vignette.wikia.nocookie.net/crashban/images/e/e9/PentaNF.png/revision/latest?cb=20190627213728) \n",
		};
	}

	updateMarkdown(markdown) {
		this.setState({ markdown });
	}

	render(){
		return (
			<div className="App">
				<div className="container">
					<div className="row mt-4">
						<Heading type="h1" text="Překladač Markdownu" />
					</div>
					<div className="row mt-4">
						<div className="col-md-6 text-center mb-4">
							<Heading type="h4" text="Vstup" />
							<div className="mark-input">
								<textarea className="input" íd="editor" value={this.state.markdown}
						            onChange={(e) => {
										this.updateMarkdown(e.target.value);
									}}
								/>
								<a href="http://www.edgering.org/markdown/" target="_blank" rel="noopener noreferrer">
									Syntaxe markdownu
								</a>
							</div>
						</div>
						<div className="col-md-6 text-center mb-4">
							<Heading type="h4" text="Náhled" />
							<div className="output text-left" id="preview"
								dangerouslySetInnerHTML={{ __html: marked(this.state.markdown) }}
							/>
							<button className="btn btn-outline-danger"
						        onClick={(e) => {
							        this.updateMarkdown('');
						        }}>
								Smazat
							</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
