import React from "react";
import Badge from "react-bootstrap/Badge";

export default class Heading extends React.Component {
	render(){
		const TAG = this.props.type;
		const VARIANT = this.props.type === 'h1' ? 'primary' : 'secondary';

		return (
			<div className="col text-center">
				<TAG>
					<Badge className="text-align-center" variant={VARIANT}>
						{this.props.text}
					</Badge>
				</TAG>
			</div>
		);
	}
};
